package com.example.demo12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DoctorController {
    @Autowired
    DoctorService doctorService;
    @PostMapping("kbc")
    public ResponseEntity<Doctor>buddy(@RequestBody Doctor doctor)
    {
        Doctor nn=doctorService.saveDoctor(doctor);
        return ResponseEntity.status(HttpStatus.OK).body(nn);
    }
}
