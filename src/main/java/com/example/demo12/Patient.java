package com.example.demo12;

import javax.persistence.*;

@Entity
@Table(name="Patient")

public class Patient {
    private Integer Id;
    private String name;
    private String email;



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }
    @Column(name="Pat_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Column(name="Pat_email")//entity class
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
