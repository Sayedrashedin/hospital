package com.example.demo12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
public class PatientController {
    @Autowired
    PatientSevice patientSevice;


    @GetMapping("/getPatientdetails")
//    @ResponseBody
    public List<Patient > getPatientdetails(Integer id)
    {
        return patientSevice.getPatient();

    }
    @PostMapping("/abc")
    public ResponseEntity<Patient >okay(@RequestBody Patient patient)
    {
        Patient pp=patientSevice.savePatient(patient);
        return ResponseEntity.status(HttpStatus.OK).body(pp);
    }
}

