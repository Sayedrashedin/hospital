package com.example.demo12;

import javax.persistence.*;

@Entity
@Table(name="Doctors")
public class Doctor {
    private Integer Idd;
    private String name;
    private String email;


    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Doctor_seq")//.customise do see
    public Integer getIdd() {
        return Idd;
    }

    public void setIdd(Integer idd) {
        Idd = idd;
    }

    @Column(name="Doc_name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="Doc_email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}




