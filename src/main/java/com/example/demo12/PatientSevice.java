package com.example.demo12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientSevice  {
    @Autowired
    PatientRepository patientRepository;
    public Patient savePatient(Patient patient)
    {
        return patientRepository.save(patient);
    }
    public List<Patient> getPatient()
    {
        return patientRepository.findAll();
    }

}
